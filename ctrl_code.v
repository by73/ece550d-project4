module ctrl_code(opcode,aluctrl,Rwe,ALUinB,DMwe,Rwd,alu_op);
	input [4:0]opcode,aluctrl;
	output Rwe,ALUinB,ALUop,DMwe,Rwd,alu_op;
	
	always @(opcode)
		begin
		case(opcode)
			//R type add,sub,adn,or,sll,sra
			5'b00000:
				begin
				Rwe <= 1'b1;
			   ALUinB <= 1'b0;
				DMwe <= 1'b0;
				Rwd <= 1'b0;
				alu_op <= aluctrl;
				end
			//addi
			5'b00101:
				begin
				Rwe <= 1'b1;
			   ALUinB <= 1'b1;
				DMwe <= 1'b0;
				Rwd <= 1'b0;
				alu_op <= 5'b00000;
				end
			//sw
			5'b00111:
				begin
				Rwe <= 1'b1;
			   ALUinB <= 1'b1;
				DMwe <= 1'b1;
				Rwd <= 1'b1;
				alu_op <= 5'b00000;
				end
			//lw
			5'b01000:
				begin
				Rwe <= 1'b1;
			   ALUinB <= 1'b1;
				DMwe <= 1'b0;
				Rwd <= 1'b1;			
				alu_op <= 5'b00000;
				end
		endcase
	end
endmodule
				